// fonction multiplier prix et quantité
function Multiply(nb1, nb2)
{
  return Number(nb1)*Number(nb2);
}

// Ajouter un autre élément.
function ToDoPlus() {
  //Creation des différents types support
  var li = document.createElement("li");
  var liQt = document.createElement("input");
  liQt.setAttribute("type", "number");
  liQt.setAttribute("value", document.getElementById("quantite").value);
  var liPrix = document.createElement("li");

  //Initialisation des valeurs mis dans les inputs
  var ajouteTache = document.getElementById("todolist").value;
  var ajouteQt = document.getElementById("quantite").value;
  var ajoutePrix = document.getElementById("Prix").value;

  //Calcul du prix 
  var prixTotal = Multiply(ajouteQt, ajoutePrix);

  var t = document.createTextNode(ajouteTache);
  var u = document.createTextNode(ajouteQt);
  var v = document.createTextNode(prixTotal);

  li.appendChild(t);
  liQt.appendChild(u);
  liPrix.appendChild(v);

  /// s'assurer que l'on met quelque-chose dans la liste
  if (ajouteTache ==='' || ajouteQt ==="" || ajoutePrix===""){
    alert("Hey, met quelque-chose quand même. :(")
  } else {
    //mettre quelque-chose dans la liste
    document.getElementById("myUL").appendChild(li);
    document.getElementById("Qt").appendChild(liQt);
    document.getElementById("prix").appendChild(liPrix);
  }
  // revoyer un élément par l'id dans la todolist et renommer par la valeur donnée
  document.getElementById("todolist").value = "";
  document.getElementById("quantite").value = "";
  document.getElementById("Prix").value = "";

//créer l'élément dans la liste et une croix pour supprimer
var span = document.createElement("SPAN");
var txt = document.createTextNode("\u00D7");
span.className = "close";
  //associer l'élément à un enfant ou un parent, voir le déplacer si l'enfant ou le parent est placé ailleurs
  span.appendChild(txt);
  li.appendChild(span);

  // Pour dire que l'élément est fait
  var list = document.querySelector('ul');
//ev est l'appel de l'élément click
list.addEventListener('click', function(ev) {
  if (ev.target.tagName === 'LI') {
    ev.target.classList.toggle('checked');
  }
}, false);

// fermer/ enlever un élement dans la liste
var close = document.getElementsByClassName("close");
var i;
for (i = 0; i < close.length; i++) {
    // on active la fonction de fermer l'élément i en cliquant dessus
    close[i].onclick = function() {
      var div = this.parentElement;
      div.style.display = "none";
    }
  }
}


